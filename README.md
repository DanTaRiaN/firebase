git https://git-scm.com/downloads
firebase https://firebase.google.com/
google sdk https://cloud.google.com/sdk/docs/quickstarts

gcloud components update
gcloud config set project <your firebase project name>
gcloud auth login
gcloud firebase test android models list
gcloud firebase test android run --app <path> --type instrumentation --test <path> --device-ids flounder,hammerhead --os-version-ids 19,21 --locales en,fr --orientations portrait,landscape