package org.testing.workshop.qag

import androidx.test.ext.junit.runners.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*


data class TestCase(val expected: Boolean, val username: String)

@RunWith(AndroidJUnit4::class)
class ValidInstrumentedTest {

    @Test
    fun EmailValidIsCorrect(){
        val emailValidCases = listOf(
            TestCase(true, "username@domain"),
            TestCase(true, "username@domain.domain"),
            TestCase(true, "username.username@domain"),
            TestCase(true, "username.username@domain.domain"),
            TestCase(true, "username.username.username@domain"),
            TestCase(true, "username.username.username@domain.domain"),
            TestCase(true, "@domain"),
            TestCase(true, "@domain.doman"),
            TestCase(false, "username.domain"),
            TestCase(false, "username.username.domain"),
            TestCase(false, "..domain")
        )

        for (testCase in emailValidCases) {
            assertEquals(
                testCase.username,
                testCase.expected,
                Utils.verificationEmail(testCase.username)
            )
        }
    }
}