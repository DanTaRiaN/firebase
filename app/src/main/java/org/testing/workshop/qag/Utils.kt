package org.testing.workshop.qag

class Utils {
    companion object {
        fun verificationEmail(email: String): Boolean {
            if (!email.contains("@") || email.length < 5) {
                return false
            }
            return true
        }

        fun verificationPassword(password: String): Boolean {
            if (password.length < 5) {
                return false
            }
            return true
        }
    }
}